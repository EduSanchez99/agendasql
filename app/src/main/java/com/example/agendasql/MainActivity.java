package com.example.agendasql;

import androidx.appcompat.app.AppCompatActivity;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import Database.AgendaDbHelper;

    public class MainActivity extends AppCompatActivity {
        private TextView lblRespuesta;
        private Button btnRespuesta;
        private Button btnCerrar;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            lblRespuesta = (TextView) findViewById(R.id.lblRespuesta);
            btnRespuesta = (Button) findViewById(R.id.btnRespuesta);
            btnCerrar = (Button) findViewById(R.id.btnCerrar);

            btnCerrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    finish();
                }
            });
            btnRespuesta.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    String dataBaseName="";

                    SQLiteDatabase db;
                    AgendaDbHelper helper = new AgendaDbHelper(MainActivity.this);
                    db = helper.getWritableDatabase();
                    helper.onUpgrade(db, 1, 2);
                    dataBaseName = helper.getDatabaseName();
                    lblRespuesta.setText(dataBaseName);
                }
            });
        }
    }
